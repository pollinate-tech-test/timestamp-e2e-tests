package integration

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/joho/godotenv"

	"github.com/google/uuid"
)

// var Cfg = Config{
// 	CommandService: struct {
// 		ServiceEndpoint
// 	}{
// 		ServiceEndpoint: ServiceEndpoint{
// 			protocol: "http",
// 			host:     "localhost",
// 			port:     "7080",
// 			actuator: "/health",
// 		},
// 	},
// 	ConsumerService: struct {
// 		ServiceEndpoint
// 	}{
// 		ServiceEndpoint: ServiceEndpoint{
// 			protocol: "http",
// 			host:     "localhost",
// 			port:     "7081",
// 			actuator: "/health",
// 		},
// 	},
// 	QueryService: struct {
// 		ServiceEndpoint
// 	}{
// 		ServiceEndpoint: ServiceEndpoint{
// 			protocol: "http",
// 			host:     "localhost",
// 			port:     "7082",
// 			actuator: "/health",
// 		},
// 	},
// }

type Config struct {
	CommandService  ServiceEndpoint
	ConsumerService ServiceEndpoint
	QueryService    ServiceEndpoint
}

func getConfig() Config {
	dotFile := ".env"
	if _, err := os.Stat(dotFile); !errors.Is(err, os.ErrNotExist) {
		err := godotenv.Load(dotFile)
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	return Config{
		CommandService: ServiceEndpoint{
			protocol: getString("COMMAND_SERVICE_PROTOCOL"),
			host:     getString("COMMAND_SERVICE_PORT"),
			port:     getString("COMMAND_SERVICE_HOST"),
			actuator: getString("COMMAND_SERVICE_ACTUATOR"),
		},
		ConsumerService: ServiceEndpoint{
			protocol: getString("CONSUMER_SERVICE_PROTOCOL"),
			host:     getString("CONSUMER_SERVICE_PORT"),
			port:     getString("CONSUMER_SERVICE_HOST"),
			actuator: getString("CONSUMER_SERVICE_ACTUATOR"),
		},
		QueryService: ServiceEndpoint{
			protocol: getString("QUERY_SERVICE_PROTOCOL"),
			host:     getString("QUERY_SERVICE_PORT"),
			port:     getString("QUERY_SERVICE_HOST"),
			actuator: getString("QUERY_SERVICE_ACTUATOR"),
		},
	}
}

func getString(key string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	log.Fatalf(key + " not found in environment")
	return ""
}

type ServiceEndpoint struct {
	protocol string
	host     string
	port     string
	actuator string
}

func (s ServiceEndpoint) endpointPath() string {
	return fmt.Sprintf("%s://%s:%s", s.protocol, s.host, s.port)
}

func (s ServiceEndpoint) healthPath() string {
	return fmt.Sprintf("%s%s", s.endpointPath(), s.actuator)
}

func (s ServiceEndpoint) health() (*http.Response, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", s.healthPath(), nil)
	if err != nil {
		return nil, err
	}
	correlationId, err := uuid.NewUUID()
	if err != nil {
		return nil, err
	}
	req.Header.Add("correlationId", correlationId.String())
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("invalid status recieved: %d from endpoint %s", res.StatusCode, s.healthPath())
	}
	return res, nil
}

func check(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err)
	}
}
